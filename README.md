# What it is

This project uses [Angular](https://angular.io/) a [NestJS](https://docs.nestjs.com/) with full [Typescript](https://www.typescriptlang.org/) support.

# What you need

Currently this thing uses mariadb (XAMMP) with a simple local connection to a database.

[Config File](/blob/master/backend/src/app.module.ts#L11)

The connection is established via [TypeORM](http://typeorm.io/) and [NestTypeORM](https://github.com/nestjs/typeorm)

# How you run it

- Go into the working folder and run `npm run watch`. This will build a compiled version of your frontend for NestJS to serve.
- Go into the [backend](/blob/master/backend/) Folder and run `npm run webpack`. More information for [webpack](https://webpack.js.org/). This will basically watch the backend stuff and make your application hot reload-able.
- Lastly serve your content in the backend with `npm run start`.

# ToDo
- Get [LDAP](/blob/master/backend/src/auth/auth.service.ts) running...

# More Information

## Angular

- [Angular Basics](https://www.youtube.com/watch?v=IZEolKjcjks&list=PL0vfts4VzfNiX5kG1Q8pg2JQ_SB0_ADM1)
- [Angular Universal](https://www.youtube.com/watch?v=wij2-gyG12E)

## Passport
- [Passport](http://www.passportjs.org/)
- [LDAP Auth](http://www.passportjs.org/packages/passport-ldapauth/)
- [Nest and Passport](https://github.com/nestjs/passport)

## Misc
- [Good Code](https://www.youtube.com/watch?v=Mus_vwhTCq0)