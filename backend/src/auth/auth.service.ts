import { Injectable } from '@nestjs/common';
import passport = require('passport');
import LdapStrategy = require('passport-ldapauth');

@Injectable()
export class AuthService {
  pass: passport.Authenticator;
  constructor() {
    this.pass = passport.use(new LdapStrategy({
      server: {
        url: 'ldap://bku.db.de:389',
        bindDN: 'cn=root',
        bindCredentials: 'secret',
        searchBase: 'ou=passport-ldapauth',
        searchFilter: '(uid={{username}})',
      },
    }));
  }

  async validateUser(user: string): Promise<any> {
    // tslint:disable-next-line:only-arrow-functions
    this.pass.authenticate(user, function(respone) {
      // tslint:disable-next-line:no-console
      console.log(respone);
    });
  }
}