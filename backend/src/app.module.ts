import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FrontendMiddleware } from './modules/common/middleware/frontend/frontend.middleware';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { PhotoModule } from './photo/photo.module';
import { Photo } from './photo/photo.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      keepConnectionAlive: true,
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'universal',
      password: 'DVTllU31PBmIgawN',
      database: 'universal',
      synchronize: true,
      entities: [
        Photo,
      ],
      migrations: [__dirname + '/migration/**/*.ts'],
      subscribers: [__dirname + '/subscriber/**/*.ts'],
    }),
    PhotoModule,
    AuthModule],
  controllers: [AppController],
  providers: [],
})

export class ApplicationModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer.apply(FrontendMiddleware).forRoutes(
      {
        path: '/**', // For all routes
        method: RequestMethod.ALL, // For all methods
      },
    );
  }
}